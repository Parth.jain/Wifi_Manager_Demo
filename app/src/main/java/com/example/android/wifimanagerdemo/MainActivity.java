package com.example.android.wifimanagerdemo;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button mEnableBtn,mDisableBtn,mAvailableWifiBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEnableBtn=findViewById(R.id.enableBtn);
        mDisableBtn=findViewById(R.id.disableBtn);
        mAvailableWifiBtn=findViewById(R.id.available_wifiBtn);
        final WifiManager mWifi= (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mEnableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWifi.setWifiEnabled(true);
            }
        });
        mDisableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWifi.setWifiEnabled(false);
            }
        });

        mAvailableWifiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,AvailableWifiActivity.class));
            }
        });
    }
}
